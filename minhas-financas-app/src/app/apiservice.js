import axios from "axios"

const httpClient = axios.create({
    baseURL: 'http://localhost:8080'
})

class ApiService {

    constructor(apiUrl) {
        this.apiUrl = apiUrl
    }

    post(url, objeto) {
        
        return httpClient.post(this.requisitaUrl(url), objeto)
    }

    put(url, objeto) {
        return httpClient.put(this.requisitaUrl(url), objeto)
    }

    delete(url) {
        return httpClient.delete(this.requisitaUrl(url))
    }

    get(url) {
        return httpClient.get(this.requisitaUrl(url))
    }

    requisitaUrl(url) {
        return `${this.apiUrl}${url}`
    }
}


export default ApiService