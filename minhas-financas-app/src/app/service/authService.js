import LocalstorateService from "./localSotrageService"

export const USUARIO_LOGADO = '_usuario_logado'

class AuthService {

    static isUsuarioAutenticado() {

        const usuario = LocalstorateService.obterItem(USUARIO_LOGADO)
        return usuario && usuario.id

    }

    static removerUsuarioAutenticado() {
        LocalstorateService.removerItem(USUARIO_LOGADO)
    }
    
    static logar(usuario) {
        LocalstorateService.adicionarItem(USUARIO_LOGADO, usuario)
    }

    static obterUsuarioAutenticado() {
        return LocalstorateService.obterItem(USUARIO_LOGADO)
    }

}

export default AuthService