class LocalstorateService {
  static adicionarItem(chave, valor) {
    return localStorage.setItem(chave, JSON.stringify(valor));
  }

  static obterItem(chave) {
    return JSON.parse(localStorage.getItem(chave));
  }

  static removerItem(chave) {
    localStorage.removeItem(chave)
  }
}

export default LocalstorateService