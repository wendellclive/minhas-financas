import React from "react";
import Card from "../componentes/card";
import FormGroup from "../componentes/form-group";
import { withRouter } from "react-router-dom";
import UsuarioService from "../app/service/usuarioService";
import { mensagemSucesso, mensagemErro } from "../componentes/toast";

class CadastroUsuario extends React.Component {
  state = {
    nome: "",
    email: "",
    senha: "",
    senhaRepeticao: "",
  };

  constructor() {
    super();
    this.service = new UsuarioService();
  }

  cadastrar = () => {
    const { nome, email, senha, senhaRepeticao } = this.state;

    const usuario = {
      nome,
      email,
      senha,
      senhaRepeticao,
    };

    try {
      this.service.validar(usuario)
    } catch (erro) {
      const msg = erro.mensagens;
      msg.forEach(msg => {
        mensagemErro(msg)
        return false
      });
      
    }
    this.service
      .salvar(usuario)
      .then((response) => {
        mensagemSucesso("Usuário cadastrado com sucesso!");
        this.props.history.push("/login");
      })
      .catch((error) => {
        mensagemErro(error.response.data);
      });
  };

  cancelar = () => {
    this.props.history.push("/login");
  };

  render() {
    return (
      <Card title="Cadastro de Usuário">
        <div className="row">
          <div className="col-lg-12">
            <div className="bs-component">
              <FormGroup label="Nome: *" htmlFor="inputNome">
                <input
                  className="form-control"
                  type="text"
                  id="inputNome"
                  name="nome"
                  onChange={(e) => this.setState({ nome: e.target.value })}
                />
              </FormGroup>
              <FormGroup label="Email: *" htmlFor="inputEmail">
                <input
                  className="form-control"
                  type="email"
                  id="inputEmail"
                  name="email"
                  onChange={(e) => this.setState({ email: e.target.value })}
                />
              </FormGroup>
              <FormGroup label="Senha: *" htmlFor="inputSenha">
                <input
                  className="form-control"
                  type="password"
                  id="inputsenha"
                  name="senha"
                  onChange={(e) => this.setState({ senha: e.target.value })}
                />
              </FormGroup>
              <FormGroup label="Repetir Senha: *" htmlFor="inputRepitaSenha">
                <input
                  className="form-control"
                  type="password"
                  id="inputRepitaSenha"
                  name="RepitaSenha"
                  onChange={(e) =>
                    this.setState({ senhaRepeticao: e.target.value })
                  }
                />
              </FormGroup>

              <button
                type="button"
                onClick={this.cadastrar}
                className="btn btn-success"
              >
                <i className="pi pi-save mr-2"></i> Salvar
              </button>
              <button
                type="button"
                onClick={this.cancelar}
                className="btn btn-danger"
              >
                <i className="pi pi-times mr-2"></i> Cancelar
              </button>
            </div>
          </div>
        </div>
      </Card>
    );
  }
}

export default withRouter(CadastroUsuario);
