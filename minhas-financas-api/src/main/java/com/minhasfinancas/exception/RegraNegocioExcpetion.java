package com.minhasfinancas.exception;

public class RegraNegocioExcpetion extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public RegraNegocioExcpetion(String msg) {
		super(msg);
	}

}
